import { useEffect, useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import TableRow from './TableRow'

function App() {
	const [users, setUsers] = useState([])

	const fetchUsers = async () => {
		const data = await (await fetch("https://jsonplaceholder.typicode.com/users")).json();
		setUsers(data)
	}

	const removeUser = (id) => {
		setUsers(users.filter(user => {
			return user.id != id;
		}));
	}

	useEffect(() => {
		fetchUsers();
	}, [])

	return (
		<div className="App">
			<table>
				<thead>
					<tr>
						<th>Id</th>
						<th>Avatar</th>
						<th>Username</th>
						<th>E-mail</th>
						<th>City</th>
						<th>Has Company</th>
					</tr>
				</thead>
				<tbody>
					{users.map((user) => {
						return (
							<TableRow key={user.id} data={user} onDeleteUser={removeUser} />
						);
					})}
				</tbody>
			</table>
		</div>
	)
}

export default App
