import React, { useEffect, useState } from "react";
import Input from "./Input";

function TableRow({ data, onDeleteUser }) {
    const [user, setUser] = useState(data);
    const [userEdited, setUserEdited] = useState(data);
    const [completedTodos, setCompletedTodos] = useState(null);
    const [isSelected, setIsSelected] = useState(false);
    const [isEditable, setIsEditable] = useState(false);

    const fetchTodos = async () => {
        const data = await (await fetch(`https://jsonplaceholder.typicode.com/users/${user.id}/todos`)).json();
        setCompletedTodos(data.filter(todo => todo.completed == true).length);
    }

    useEffect(() => {
        fetchTodos();
    }, []);

    const toggleSelection = () => {
        setIsSelected(!isSelected);
    }

    const toggleEdition = () => {
        setIsEditable(!isEditable);
    }

    const deleteRow = () => {
        onDeleteUser(user.id)
        toggleSelection();
    }

    // The 'user' object contains properties that are objects themselves; and since the 'user' only contains a maximum of 3 nested object i found it simple to write the function this way, instead of using something like recursion.
    const editValue = (property, subproperty, subsubproperty, value) => {
        if (subsubproperty) {
            setUserEdited({ ...userEdited, [property]: { ...userEdited[property], [subproperty]: { ...userEdited[property][subproperty], [subsubproperty]: value } } });
        }
        else if (subproperty) {
            setUserEdited({ ...userEdited, [property]: { ...userEdited[property], [subproperty]: value } });
        } else {
            setUserEdited({ ...userEdited, [property]: value });
        }
    }

    // Used a copy of the 'user' object so the table doesn't update on every input change; this way it only changes when the 'Save' button is pressed.
    const saveEdition = () => {
        setUser({ ...userEdited });
        toggleEdition();
    }

    return (
        <React.Fragment>
            <tr className={`highlightable ${isSelected ? 'row-selected' : null}`} onClick={toggleSelection}>
                <td>{user.id}</td>
                <td><img src="https://placeholder.photo/img/50" width={50} height={50} /></td>
                <td>{user.username}</td>
                <td>{user.email}</td>
                <td>{user.address.city}</td>
                <td>{user.company ? "True" : "False"}</td>
            </tr>
            {isSelected &&
                <tr>
                    <td colSpan={6}>
                        <div className="user-details-wrapper">
                            <div>
                                <p className="details-title">User Details</p>
                                <p><b>ID</b>: {userEdited.id}</p>
                                <p className="details-line"><b>Name</b>: {isEditable ? <Input value={userEdited.name} handleOnChange={(e) => editValue('name', null, null, e.target.value)} /> : userEdited.name}</p>
                                <p className="details-line"><b>Username</b>: {isEditable ? <Input value={userEdited.username} handleOnChange={(e) => editValue('username', null, null, e.target.value)} /> : userEdited.username}</p>
                                <p className="details-line"><b>E-mail</b>: {isEditable ? <Input value={userEdited.email} handleOnChange={(e) => editValue('email', null, null, e.target.value)} /> : userEdited.email}</p>
                                <p className="details-line"><b>Phone</b>: {isEditable ? <Input value={userEdited.phone} handleOnChange={(e) => editValue('phone', null, null, e.target.value)} /> : userEdited.phone}</p>
                                <p className="details-line"><b>Website</b>: {isEditable ? <Input value={userEdited.website} handleOnChange={(e) => editValue('website', null, null, e.target.value)} /> : userEdited.website}</p>
                                <p className="details-line"><b>Completed todos</b>: {completedTodos ?? 'N/A'}</p>
                            </div>
                            <div>
                                <p className="details-title">Address</p>
                                <p className="details-line"><b>Street</b>: {isEditable ? <Input value={userEdited.address.street} handleOnChange={(e) => editValue('address', 'street', null, e.target.value)} /> : userEdited.address.street}</p>
                                <p className="details-line"><b>Suite</b>: {isEditable ? <Input value={userEdited.address.suite} handleOnChange={(e) => editValue('address', 'suite', null, e.target.value)} /> : userEdited.address.suite}</p>
                                <p className="details-line"><b>City</b>: {isEditable ? <Input value={userEdited.address.city} handleOnChange={(e) => editValue('address', 'city', null, e.target.value)} /> : userEdited.address.city}</p>
                                <p className="details-line"><b>Zipcode</b>: {isEditable ? <Input value={userEdited.address.zipcode} handleOnChange={(e) => editValue('address', 'zipcode', null, e.target.value)} /> : userEdited.address.zipcode}</p>
                                <p className="details-line"><b>Geo</b>: {isEditable ? <Input value={userEdited.address.geo.lat} handleOnChange={(e) => editValue('address', 'geo', 'lat', e.target.value)} /> : userEdited.address.geo.lat} | {isEditable ? <Input value={userEdited.address.geo.lng} handleOnChange={(e) => editValue('address', 'geo', 'lng', e.target.value)} /> : userEdited.address.geo.lng}</p>
                            </div>
                            <div>
                                <p className="details-title">Company</p>
                                <p className="details-line"><b>Name</b>: {isEditable ? <Input value={userEdited.company.name} handleOnChange={(e) => editValue('company', 'name', null, e.target.value)} /> : userEdited.company.name}</p>
                                <p className="details-line"><b>Catch Phrase</b>: {isEditable ? <Input value={userEdited.company.catchPhrase} handleOnChange={(e) => editValue('company', 'catchPhrase', null, e.target.value)} /> : userEdited.company.catchPhrase}</p>
                                <p className="details-line"><b>Bs</b>: {isEditable ? <Input value={userEdited.company.bs} handleOnChange={(e) => editValue('company', 'bs', null, e.target.value)} /> : userEdited.company.bs}</p>
                            </div>
                        </div>
                        <button className="btn-delete mr-15" onClick={() => deleteRow()}>Remove</button>
                        {isEditable ? (
                            <button onClick={saveEdition}>Save</button>
                        ) : (
                            <button onClick={toggleEdition}>Edit</button>
                        )}
                    </td>
                </tr>
            }
        </React.Fragment>
    );
}

export default TableRow;