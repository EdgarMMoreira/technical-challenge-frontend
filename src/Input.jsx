function Input({ value, handleOnChange }) {
    return (
        <input value={value} size={15} onChange={(e) => handleOnChange(e)} />
    )
}

export default Input;