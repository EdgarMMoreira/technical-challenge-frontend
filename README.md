# Technical Challenge – Frontend

Create listing users from an external API.

## Live Demo

https://spontaneous-gecko-b1360d.netlify.app/

## Features

* shows a list of users
* shows more user details when selecting a line
* edit user details
* remove user

## Technologies used

* Node (v18.13.0)
* NPM (v8.19.3)
* React
* Vite

## How to Use

``` bash
# Install dependencies
npm install

# Run the app
npm run dev

# Open localhost link in browser
```

## Code structure

``main.jsx``

Contains the entry for the app.

``App.jsx``

Holds the table and the users data.

``TableRow.jsx``

Represents a row on the users table; is responsible for showing all the users details.

``Input.jsx``

Represents an input element, mainly used for editing user details.